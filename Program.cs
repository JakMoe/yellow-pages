﻿using System;
using System.Collections.Generic;

namespace Yellow_Pages


{
    class Contact
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
    class Program
    {
        public static Dictionary<int, Contact> contacts =
        new Dictionary<int, Contact>()
        {

               {0, new Contact {FirstName ="Bro", LastName = "Dudeson"} },
               {1, new Contact {FirstName ="Jon", LastName ="Blund"} },
               {2, new Contact {FirstName ="Arya", LastName ="Stark"} },
               {3, new Contact {FirstName ="Bob", LastName ="TheBuilder"} },
               {4, new Contact {FirstName ="Cho", LastName ="Gath"} }
        };
        public static void RunProgram()
        {
            Console.WriteLine("Search for name: ");
            string input = Console.ReadLine();
            for (int i = 0; i < contacts.Count; i++)
            {
                string tempName = contacts[i].FirstName + " " + contacts[i].LastName;
                if (tempName.ToLower().Contains(input.ToLower()))
                {
                    Console.WriteLine("Match found! Name: " + tempName);
                }
                
            }
            Console.WriteLine("No match found try again!");
            ProgramEnd();
        }
        public static void ProgramEnd()
        {
            Console.WriteLine("Do you want to play again Y/N?: ");
            string userInput = Console.ReadLine();
            if (userInput.ToLower() == "y")
            {
                RunProgram();
            }
            else if(userInput.ToLower()=="n")
            {
                Console.WriteLine("Cya Nerd!!");
            }
            else
            {
                
            }

        }
            static void Main(string[] args)
        {
                RunProgram();
            }
        }
    }

    

